# Utilofies

## Purpose

Just a bunch of utility function I need a lot. If you like a function,
feel free to clone, copy, hug, etc.

## Caveat

Often I only need one or two functions in a given project, and I don’t
want to install all the dependencies of the functions that I don’t need,
so I don’t list them in setup.py. Below a list of libraries used in the
different modules.

## Dependencies by Module

`lrucachelib`:

- pylru

`bslib`:
    
- beautifulsoup4
