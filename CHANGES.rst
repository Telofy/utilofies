3.0 (2017-07-05)
================

- Removed Python 2 compatibility.

- Reimplemented `stdlib.isoformat` which was broken in Python 3.6. New version
  has been tested under 3.4 and 3.6.


2.4.1 (2017-06-02)
==================

- Last version in the Python2 compatible 2.x line (bugfix releases may follow).
