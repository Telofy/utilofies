# -*- encoding: utf-8 -*-
from __future__ import (unicode_literals, absolute_import,
                        print_function, division)
import codecs


MAGIC_STRING = '[utilofies decoding error]'

def replace_with_string(exc):
    return (MAGIC_STRING, exc.end)
codecs.register_error('replace-with-string', replace_with_string)


def test_cpython_crash():
    """
    A bit of an odd test because if it fails CPython just aborts like so::

        In [12]: content.decode('windows-1252', 'replace-with-string')
        *** Error in `…/bin/python3.4': realloc(): invalid next size: 0x00000000032f6fc0 ***
        Aborted
        $

    Or like so::

        $ bin/py.test utilofies
        ================================= test session starts =======================================
        platform linux -- Python 3.4.2, pytest-3.1.1, py-1.4.33, pluggy-0.4.0
        rootdir: …, inifile:
        collected 107 items

        …/tests/test_cpython_crash.py *** Error in `…/bin/python3.4': realloc(): invalid next size: 0x0000000004e4c6a0 ***
        Aborted
        $

    It’s probably a problem with the compilation.
    """
    with open('utilofies/tests/samples/cpython-crash.html', 'rb') as htmlfile:
        content = htmlfile.read()
    content.decode('windows-1252', 'replace-with-string')  # Crashes CPython 3.4 on Sfydev
