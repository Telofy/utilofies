# flake8: noqa
from datetime import datetime, timedelta, timezone
import pytest

@pytest.mark.parametrize("dt,formatted", [
    ('', None),
    (False, None),
    (None, None),
    (datetime(2017, 1, 1, 12, 59), '2017-01-01T12:59:00.000000Z'),
    (datetime(2017, 1, 1, 12, 59, 59), '2017-01-01T12:59:59.000000Z'),
    (datetime(2017, 1, 1, 12, 59, 59, 123), '2017-01-01T12:59:59.000123Z'),
    (datetime(2017, 1, 1, 12, 59, 59, 123456), '2017-01-01T12:59:59.123456Z'),
    (datetime(2017, 1, 1, 12, 0), '2017-01-01T12:00:00.000000Z'),
    (datetime(2017, 1, 1, 12, 0, tzinfo=None), '2017-01-01T12:00:00.000000Z'),
    (datetime(2017, 1, 1, 12, 0, tzinfo=timezone.utc), '2017-01-01T12:00:00.000000Z'),
    (datetime(2017, 1, 1, 12, 0, tzinfo=timezone(timedelta(hours=0))), '2017-01-01T12:00:00.000000Z'),
    (datetime(2017, 1, 1, 12, 0, tzinfo=timezone(timedelta(hours=2))), '2017-01-01T10:00:00.000000Z'),
    (datetime(2017, 1, 1, 12, 0, tzinfo=timezone(timedelta(hours=-2))), '2017-01-01T14:00:00.000000Z'),
])
def test_isoformat(dt, formatted):
    from ..stdlib import isoformat
    assert isoformat(dt) == formatted
