#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='utilofies',
    version='3.0',
    author='Denis Drescher',
    author_email='denis.drescher+utilofies@claviger.net',
    packages=find_packages(),
    include_package_data=True,
    extras_require=dict(
        test=[],
    ),
    install_requires=[
        'six',
    ],
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
    zip_safe=False,
    entry_points={
        'console_scripts': []
    }
)
